import allure
import unittest
from pageobject.pagefactory import Factory

class TestHomePage(unittest.TestCase):

    def setUp(self):
        Factory.homepage.go_to_homepage()

    @allure.title("Test homepage title")
    def test_pulp_homepage_title(self):
        assert "Pulp App Main Menu" == Factory.homepage.pulp_homepage_title.text
    @allure.step("Click table link")
        def click_table_link(self):
            self.link_table.click()

    @allure.step("Click link list")
    def click_link_list(self):
        self.link_list.click()

    @web_driver
    @allure.step("Click link faq")
    def click_link_faq(self, driver):
        self.link_faq.click()
        logs = driver.get_log('browser')
        print(logs)

    @allure.title("Test FAQs link")
    def test_FAQs_link(self):
        Factory.homepage.click_link_faq()
        assert "FAQs" in Factory.faqpage.faq_page_title.text

    @web_driver
    @allure.step("Click link faq")
    def click_link_faq(self, driver):
        self.link_faq.click()
        logs = driver.get_log('browser')
        print(logs)